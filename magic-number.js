// let
// const -> Const é uma variavel que não pode ter re atribuições (reassign)
// var
// document.addEventListener -> Se for fazer script sem DEFER

// string
// number
// boolean
// object
// undefined
// function

function reset(
    title,
    retryButton,
    tryList,
    attemptEl,
    btnMagicNumber,
    maxAttempt
){
    title.remove();
    retryButton.remove();
    tryList.innerHTML = "";
    attemptEl.innerHTML = maxAttempt;
    btnMagicNumber.removeAttribute("disabled");
    
}

function getRandowNumber(){
    const minSecretNumber = 1;
    const maxSecretNumber = 100;
    return Math.round(Math.random() * (maxSecretNumber - minSecretNumber) + minSecretNumber);      
}

function renderReset(
    titleText,
    btnMagicNumber,
    mainContainer,
    onReset
){
    const title = document.createElement("h2");
    const retryButton = document.createElement("button");
    
    title.innerHTML = titleText;
    retryButton.innerText = "Tentar Novamente";
    retryButton.addEventListener("click", function(){
        onReset(title, retryButton)
    })
    mainContainer.append (title);
    mainContainer.append(retryButton);
    btnMagicNumber.setAttribute("disabled", false);
}

document.addEventListener("DOMContentLoaded", ()  => {
    //
    //
    const btnMagicNumber = document.getElementById("btn-magic-number");
    const inputMagicNumber = document.getElementById("magic-number");
    const attemptEl = document.getElementById("attempt");
    const tryList = document.getElementById("try-list");
    const mainContainer = document.querySelector("body");
    
    const maxAttempt = 10;
    let secretNumber = getRandowNumber();
    let attempt = 0;  
    let isWinner = false;

    attemptEl.innerHTML = maxAttempt;
    
    btnMagicNumber.addEventListener("click", function() {
        attempt++;
        attemptEl.innerHTML = maxAttempt - attempt;
        const magicNumber = Math.floor(+inputMagicNumber.value); 
        inputMagicNumber.value = "";
        inputMagicNumber.focus();
        // Renderizando a lista por completo
        // tryList.innerHTML += `<li>${magicNumber}</li>`; // Interpolacao / Template String
        
        // Renderizando um item apenas
        const tryItemElement = document.createElement("li");
        tryItemElement.innerHTML = magicNumber;
        if(magicNumber > secretNumber){
            tryItemElement.classList.add("higher-number");
        } else if (magicNumber < secretNumber){
            tryItemElement.classList.add("lowest-number");
        } else {
            tryItemElement.classList.add("correct-number");
            isWinner = true;
        }
            
        tryList.append(tryItemElement)
        
        if(attempt >= 10 || isWinner){
            let titleText = isWinner ? "Você ganhou" : "Você perdeu";
            renderReset(
                titleText,
                btnMagicNumber,
                mainContainer,
                function(title, retryButton){
                    reset(
                       title,
                       retryButton,
                       tryList,
                       attemptEl,
                       btnMagicNumber,
                       maxAttempt
                    )

                    attempt = 0;
                    secretNumber = getRandowNumber();
                    isWinner = false;
                }    
            )
        }
    });  
});
